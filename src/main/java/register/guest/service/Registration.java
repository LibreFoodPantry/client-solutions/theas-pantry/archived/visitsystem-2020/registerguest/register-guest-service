package register.guest.service;

public class Registration {
	private String id;
	private String registrationDate;
	private int ageRange;
	private boolean isResident;
	private String zipCode;
	private boolean socialSecurity;
	private boolean tanfeadc;
	private boolean snap;
	private boolean wic;
	private boolean sfsp;
	private boolean schoolBreakfast;
	private boolean schoolLunch;
	private boolean financialAid;
	private String otherBenefit;
	private boolean employed;
	private int houseHoldSize;
	
	public Registration() {}
	
	public Registration(String id,String registrationDate,int ageRange,boolean isResident,String zipCode,boolean socialSecurity,boolean tanfeadc,boolean snap,boolean wic,boolean sfsp,boolean schoolBreakfast,boolean schoolLunch,boolean financialAid,String otherBenefit,boolean employed,int houseHoldSize) {
			this.id=id;
			this.registrationDate=registrationDate;
			this.ageRange=ageRange;
			this.isResident=isResident;
			this.zipCode=zipCode;
			this.socialSecurity=socialSecurity;
			this.tanfeadc=tanfeadc;
			this.snap=snap;
			this.wic=wic;
			this.sfsp=sfsp;
			this.schoolBreakfast=schoolBreakfast;
			this.schoolLunch=schoolLunch;
			this.financialAid=financialAid;
			this.otherBenefit=otherBenefit;
			this.employed=employed;
			this.houseHoldSize=houseHoldSize;
	}
	
	public String getid(){
		return id;
	}
	public String getregistrationDate(){
		return registrationDate;
	}
	public int getageRange(){
		return ageRange;
	}
	public boolean getisResident(){
		return isResident;
	}
	public String getzipCode(){
		return zipCode;
	}
	public boolean getsocialSecurity(){
		return socialSecurity;
	}
	public boolean gettanfeadc(){
		return tanfeadc;
	}
	public boolean getsnap(){
		return snap;
	}
	public boolean getwic(){
		return wic;
	}
	public boolean getsfsp(){
		return sfsp;
	}
	public boolean getschoolBreakfast(){
		return schoolBreakfast;
	}
	public boolean getschoolLunch(){
		return schoolLunch;
	}
	public boolean getfinancialAid(){
		return financialAid;
	}
	public String getotherBenefit(){
		return otherBenefit;
	}
	public boolean getemployed(){
		return employed;
	}
	public int gethouseHoldSize(){
		return houseHoldSize;
	}

	public void setid(String id){
		this.id=id;
	}
	public void setregistrationDate(String registrationDate){
		this.registrationDate=registrationDate;
	}
	public void setageRange(int ageRange){
		this.ageRange=ageRange;
	}
	public void setisResident(boolean isResident){
		this.isResident=isResident;
	}
	public void setzipCode(String zipCode){
		this.zipCode=zipCode;
	}
	public void setsocialSecurity(boolean socialSecurity){
		this.socialSecurity=socialSecurity;
	}
	public void settanfeadc(boolean tanfeadc){
		this.tanfeadc=tanfeadc;
	}
	public void setsnap(boolean snap){
		this.snap=snap;
	}
	public void setwic(boolean wic){
		this.wic=wic;
	}
	public void setsfsp(boolean sfsp){
		this.sfsp=sfsp;
	}
	public void setschoolBreakfast(boolean schoolBreakfast){
		this.schoolBreakfast=schoolBreakfast;
	}
	public void setschoolLunch(boolean schoolLunch){
		this.schoolLunch=schoolLunch;
	}
	public void setfinancialAid(boolean financialAid){
		this.financialAid=financialAid;
	}
	public void setotherBenefit(String otherBenefit){
		this.otherBenefit=otherBenefit;
	}
	public void setemployed(boolean employed){
		this.employed=employed;
	}
	public void sethouseHoldSize(int houseHoldSize){
		this.houseHoldSize=houseHoldSize;
	}
	
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
	        return true;
		}
	    if (obj == null) {
	        return false;
	    }
	    if (getClass() != obj.getClass()) {
	        return false;
	    }
	    Registration reg = (Registration) obj;
	    return (
	    		this.id==reg.getid() &&
				//this.registrationDate.equals(reg.getregistrationDate()) &&
				this.ageRange==reg.getageRange() &&
				this.isResident==reg.getisResident() &&
				this.zipCode.equals(reg.getzipCode()) &&
				this.socialSecurity==reg.getsocialSecurity() &&
				this.tanfeadc==reg.gettanfeadc() &&
				this.snap==reg.getsnap() &&
				this.wic==reg.getwic() &&
				this.sfsp==reg.getsfsp() &&
				this.schoolBreakfast==reg.getschoolBreakfast() &&
				this.schoolLunch==reg.getschoolLunch() &&
				this.financialAid==reg.getfinancialAid() &&
				this.otherBenefit.equals(reg.getotherBenefit()) &&		
				this.employed==reg.getemployed() &&
				this.houseHoldSize==reg.gethouseHoldSize()
	    		);
	    }
}
