package register.guest.service;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.TreeMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Flux;


@RestController
public class RegisterGuestServiceController {
	
	@CrossOrigin()
	@GetMapping("/registerGuest/isRegistered/{id}")
	public ResponseEntity<Object> isRegistered(@PathVariable String id) throws Exception {
		boolean result = DataBaseAccess.isRegistered(id);
		if(result==true) {
			return new ResponseEntity<>(result,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(result,HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin()
	@GetMapping("/registerGuest/isRegistered2/{id}")
	public ResponseEntity<Object> isRegistered2(@PathVariable String id) throws Exception {
		boolean output;
		WebClient client = WebClient.create("http://localhost:8080");
		try {
			Mono<String> result = client.get().uri("/registerGuest/isRegistered/"+id).retrieve().bodyToMono(String.class);
			output=Boolean.parseBoolean(result.block());
		}
		catch(Exception e){
			output=false;
		}
		if(output != false) {
			return new ResponseEntity<>(output,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(output,HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin()
	@GetMapping("/registerGuest/getRegistration/{id}")
	public ResponseEntity<Object> getRegistration(@PathVariable String id) throws Exception {
		Registration registration = DataBaseAccess.getRegistration(id);
		
		//TODO call update endpoint and apply to registration
		
		if(registration != null) {
			return new ResponseEntity<>(registration,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("No Registration With That ID",HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin()
	@GetMapping("/registerGuest/listRegistration")
	public ResponseEntity<Object> listRegistration(@RequestParam String startDate,@RequestParam String endDate) throws Exception {
		ArrayList<Registration> regList = DataBaseAccess.listRegistration(startDate,endDate);
		
		//TODO call update by date endpoint and add to regList / apply to reglist

		if(regList.size()!=0) {
			return new ResponseEntity<>(regList,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("No Registrations Within That Date Range",HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin()
	@GetMapping("/registerGuest/allRegistration")
	public ResponseEntity<Object> allRegistration() throws Exception {
		ArrayList<Registration> regList = DataBaseAccess.allRegistration();
		
		//TODO call update endpoint and add to regList / apply to reglist

		if(regList.size()!=0) {
			return new ResponseEntity<>(regList,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("No Registrations Exist",HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin()
	@PostMapping("/registerGuest/newRegisteration")
	public ResponseEntity<Object> newRegisteration(@RequestBody Registration registration) throws Exception {
		registration.setregistrationDate(LocalDate.now().toString());
		boolean result = DataBaseAccess.newRegisteration(registration);
		if(result == true) {
			return new ResponseEntity<>(result,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(result,HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin()
	@PostMapping("/registerGuest/shutdown")
	public void shutdown() throws Exception {
		DataBaseAccess.disconnect();
		System.exit(0);
	}
}