package register.guest.service;

import java.sql.*;
import java.util.ArrayList;

public class DataBaseAccess {
	private static String ip = "localhost";
	private static String port = "3306";
	private static String username = "root";
	private static String password = "password";
	
	
	private static Connection connect = null;
    private static Statement statement = null;
    private static PreparedStatement preparedStatement = null;
    private static ResultSet resultSet = null;
    
    public static void connect() throws Exception {
    	try{
    		Class.forName("com.mysql.cj.jdbc.Driver");
    		connect = DriverManager.getConnection("jdbc:mysql://"+ip+":"+port+"/registerguest",username,password);
    		statement=connect.createStatement();  
        }catch(Exception e){ 
        	System.out.println(e);
        	}
    }
    public static boolean isRegistered(String id) throws Exception {
    	String query=("SELECT * from registerguest where id="+id);
    	resultSet=statement.executeQuery(query);
    	return resultSet.next();
    }
    public static Registration getRegistration(String id) throws Exception {
    	String query=("SELECT * from registerguest where id="+id);
    	resultSet=statement.executeQuery(query);
    	Registration registration = null; 
    	if(resultSet.first()==true) {
    		registration = new Registration();
    		registration.setid(resultSet.getString("id"));
    		registration.setregistrationDate(resultSet.getString("registrationDate"));
    		registration.setageRange(resultSet.getInt("ageRange"));
    		registration.setisResident(resultSet.getBoolean("isResident"));
    		registration.setzipCode(resultSet.getString("zipCode"));
            registration.setsocialSecurity(resultSet.getBoolean("socialSecurity"));
            registration.settanfeadc(resultSet.getBoolean("tanfeadc"));
            registration.setsnap(resultSet.getBoolean("snap"));
            registration.setwic(resultSet.getBoolean("wic"));
            registration.setsfsp(resultSet.getBoolean("sfsp"));
            registration.setschoolBreakfast(resultSet.getBoolean("schoolBreakfast"));
            registration.setschoolLunch(resultSet.getBoolean("schoolLunch"));
            registration.setfinancialAid(resultSet.getBoolean("financialAid"));
            registration.setotherBenefit(resultSet.getString("otherBenefit"));
            registration.setemployed(resultSet.getBoolean("employed"));
            registration.sethouseHoldSize(resultSet.getInt("householdSize"));
    	}
    	return registration;

    }
    public static ArrayList<Registration> listRegistration(String startDate,String endDate) throws Exception {
    	startDate = startDate.replace("-","");
    	endDate = endDate.replace("-","");
    	String query=("SELECT * from registerguest WHERE registrationDate between "+startDate+" And "+endDate);
    	resultSet=statement.executeQuery(query);
    	ArrayList<Registration> regList = new ArrayList<Registration>();
    	while(resultSet.next()==true) {
    		Registration registration = new Registration();
    		registration.setid(resultSet.getString("id"));
    		registration.setregistrationDate(resultSet.getString("registrationDate"));
    		registration.setageRange(resultSet.getInt("ageRange"));
    		registration.setisResident(resultSet.getBoolean("isResident"));
    		registration.setzipCode(resultSet.getString("zipCode"));
            registration.setsocialSecurity(resultSet.getBoolean("socialSecurity"));
            registration.settanfeadc(resultSet.getBoolean("tanfeadc"));
            registration.setsnap(resultSet.getBoolean("snap"));
            registration.setwic(resultSet.getBoolean("wic"));
            registration.setsfsp(resultSet.getBoolean("sfsp"));
            registration.setschoolBreakfast(resultSet.getBoolean("schoolBreakfast"));
            registration.setschoolLunch(resultSet.getBoolean("schoolLunch"));
            registration.setfinancialAid(resultSet.getBoolean("financialAid"));
            registration.setotherBenefit(resultSet.getString("otherBenefit"));
            registration.setemployed(resultSet.getBoolean("employed"));
            registration.sethouseHoldSize(resultSet.getInt("householdSize"));
    		regList.add(registration);	
    		}
    		return regList;
    }
    public static ArrayList<Registration> allRegistration() throws Exception {
    	String query=("SELECT * from registerguest");
    	resultSet=statement.executeQuery(query);
    	ArrayList<Registration> regList = new ArrayList<Registration>();
    	while(resultSet.next()==true) {
    		Registration registration = new Registration();
    		registration.setid(resultSet.getString("id"));
    		registration.setregistrationDate(resultSet.getString("registrationDate"));
    		registration.setageRange(resultSet.getInt("ageRange"));
    		registration.setisResident(resultSet.getBoolean("isResident"));
    		registration.setzipCode(resultSet.getString("zipCode"));
            registration.setsocialSecurity(resultSet.getBoolean("socialSecurity"));
            registration.settanfeadc(resultSet.getBoolean("tanfeadc"));
            registration.setsnap(resultSet.getBoolean("snap"));
            registration.setwic(resultSet.getBoolean("wic"));
            registration.setsfsp(resultSet.getBoolean("sfsp"));
            registration.setschoolBreakfast(resultSet.getBoolean("schoolBreakfast"));
            registration.setschoolLunch(resultSet.getBoolean("schoolLunch"));
            registration.setfinancialAid(resultSet.getBoolean("financialAid"));
            registration.setotherBenefit(resultSet.getString("otherBenefit"));
            registration.setemployed(resultSet.getBoolean("employed"));
            registration.sethouseHoldSize(resultSet.getInt("householdSize"));
    		regList.add(registration);	
    		}
    		return regList;
    }
    public static boolean newRegisteration(Registration registration) throws Exception {
    	if(DataBaseAccess.isRegistered(registration.getid())==false) {
    		String query=("INSERT INTO registerguest Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        	preparedStatement = connect.prepareStatement(query);
        	preparedStatement.setString(1, registration.getid());
        	preparedStatement.setDate(2, java.sql.Date.valueOf(registration.getregistrationDate()));
        	preparedStatement.setInt(3, registration.getageRange());
        	preparedStatement.setBoolean(4, registration.getisResident());
        	preparedStatement.setString(5,registration.getzipCode());
        	preparedStatement.setBoolean(6, registration.getsocialSecurity());
        	preparedStatement.setBoolean(7,registration.gettanfeadc());
        	preparedStatement.setBoolean(8, registration.getsnap());
        	preparedStatement.setBoolean(9, registration.getwic());
            preparedStatement.setBoolean(10, registration.getsfsp());
            preparedStatement.setBoolean(11, registration.getschoolBreakfast());
            preparedStatement.setBoolean(12, registration.getschoolLunch());
            preparedStatement.setBoolean(13, registration.getfinancialAid());
            preparedStatement.setString(14, registration.getotherBenefit());
            preparedStatement.setBoolean(15, registration.getemployed());
            preparedStatement.setInt(16, registration.gethouseHoldSize());
            preparedStatement.execute();
            return true;
    	}
    	else {
    		return false;
    	}
    }
    public static void disconnect() throws Exception {
    	try {
    		connect.close();
    	}catch(Exception e){
    		System.out.println(e);
    	}
    }
}
